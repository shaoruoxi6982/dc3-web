import Vue from 'vue'
import VueRouter from 'vue-router'
import CommonRouter from './common/index'
import ViewsRouter from './views/index'
import NProgress from "nprogress";

Vue.use(VueRouter);

const router = new VueRouter({
    scrollBehavior: (to, from, savedPosition) => {
        if (savedPosition) {
            return savedPosition
        } else {
            if (from.meta.keepAlive) {
                from.meta.savedPosition = document.body.scrollTop;
            }
            return {
                x: 0,
                y: to.meta.savedPosition || 0
            }
        }
    },
    routes: []
});

router.beforeEach((to, from, next) => {
    NProgress.start();
    const meta = to.meta || {};
    if (meta.title) {
        document.title = to.meta.title
    }
    next();
});

router.afterEach(() => {
    NProgress.done();
});

router.addRoutes([...CommonRouter, ...ViewsRouter]);

export default router
